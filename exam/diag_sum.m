function x = diag_sum(X)
[m,n] = size(X) ;
a = min(m,n) ;
b = (a+1)/2 ;
X = X([1:a],[1:a]) ;
Y = flip(X) ;
if mod(a,2) == 0
     x = sum(diag(X))+sum(diag(Y)) ;
else
     x = sum(diag(X))+sum(diag(Y))-X(b,b) ;
end
end