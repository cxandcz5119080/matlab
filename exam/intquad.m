function Q21 = intquad (n)
A1 = ones (n) ;
Q21 = [ A1 * -1 , A1 * exp(1); A1*pi, A1 ]  ;
end